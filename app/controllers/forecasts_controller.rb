class ForecastsController < ApplicationController
  before_action :set_forecast, only: [:show, :edit, :update, :destroy]

  # GET /forecasts
  # GET /forecasts.json
  def index
    @forecasts = Forecast.last(10)
    @forecast = Forecast.new

=begin    @response = RestClient.get 'api.openweathermap.org/data/2.5/weather', 
                {:params => {'zip' => :zip, 
                             'countrycode' => 'us',
                             'units' => 'imperial', 
                             'APPID' => 'be6738da9d03b99569232206aebfcde2'}}

    @parsed = JSON.parse(@response)



    @forecast.current_temp = @parsed["main"]["temp"]
    @forecast.high_low = @parsed["main"]["temp_max"]  
    @forecast.wind = @parsed["wind"]["speed"] 
    @forecast.last_checked = DateTime.now
    #@forecast.save
=end  
  end

  # GET /forecasts/1
  # GET /forecasts/1.json
  def show
  end

  # GET /forecasts/new
  def new
    @forecast = Forecast.new
  end

  # GET /forecasts/1/edit
  def edit
  end

  # POST /forecasts
  # POST /forecasts.json
  def create

     @response = RestClient.get 'api.openweathermap.org/data/2.5/weather', 
                {:params => {'zip' => forecast_params[:zip], 
                             'countrycode' => 'us',
                             'units' => 'imperial', 
                             'APPID' => 'be6738da9d03b99569232206aebfcde2'}}

@parsed = JSON.parse(@response)


    @forecast = Forecast.new(forecast_params)
    @forecast.current_temp = @parsed["main"]["temp"]
    @forecast.high_low = @parsed["main"]["temp_max"]  
    @forecast.wind = @parsed["wind"]["speed"] 
    @forecast.last_checked = DateTime.now
    
    respond_to do |format|
      if @forecast.save
        format.html { redirect_to @forecast, notice: 'Forecast was successfully created.' }
        format.json { render :show, status: :created, location: @forecast }
      else
        format.html { render :new }
        format.json { render json: @forecast.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forecasts/1
  # PATCH/PUT /forecasts/1.json
  def update
    respond_to do |format|
      if @forecast.update(forecast_params)
        format.html { redirect_to @forecast, notice: 'Forecast was successfully updated.' }
        format.json { render :show, status: :ok, location: @forecast }
      else
        format.html { render :edit }
        format.json { render json: @forecast.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forecasts/1
  # DELETE /forecasts/1.json
  def destroy
    @forecast.destroy
    respond_to do |format|
      format.html { redirect_to forecasts_url, notice: 'Forecast was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forecast
      @forecast = Forecast.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forecast_params
      params.require(:forecast).permit(:zip, :current_temp, :high_low, :wind, :last_checked)
    end
end
