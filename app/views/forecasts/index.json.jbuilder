json.array!(@forecasts) do |forecast|
  json.extract! forecast, :id, :zip, :current_temp, :high_low, :wind, :last_checked
  json.url forecast_url(forecast, format: :json)
end
