json.extract! @forecast, :id, :zip, :current_temp, :high_low, :wind, :last_checked, :created_at, :updated_at
