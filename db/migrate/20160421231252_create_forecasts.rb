class CreateForecasts < ActiveRecord::Migration
  def change
    create_table :forecasts do |t|
      t.string :zip
      t.string :current_temp
      t.string :high_low
      t.string :wind
      t.datetime :last_checked

      t.timestamps null: false
    end
  end
end
